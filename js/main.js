$(function () {
	var  profileData;
	var lastMessageDate;
	var url = 'http://146.185.154.90:8000/blog/a.detenkov@gmail.com/';
	var popupForEdit = $('.editPopup');
	var overlay = $('.overlay');
	var popupForFollow = $('.signUp');

	function getName() {
		$.ajax({
			url: url + 'profile',
			method: 'get',
			success: function (data) {
				profileData = data;
			}
		}).then(function (profile) {
			$('#name').text(profile.firstName + ' ' + profile.lastName);
		});
	}
	getName();

	function sendDataToServer(){
		var message = $('#message').val();
		if (message !== ''){
			$.ajax({
				url: url + 'posts',
				method: 'POST',
				data: {message: message}
			});
		} else alert('You have not text to post');
		$('#message').val('');
	}

	$('#send').on('click', sendDataToServer);

	$.ajax({
		url: url + 'posts',
		method: 'get'
	}).then(function showData(data){
		viewMessages(data);
		setInterval(function(){
			$.ajax({
				url: url + 'posts?datetime=' + lastMessageDate,
				method: 'get',
				success: viewMessages
			});
		}, 3000);
	});

	function viewMessages(data){
		if(data.length > 0){
			lastMessageDate = data[data.length-1].datetime;
			var lastPosts = data.splice(-20);
			lastPosts.forEach(function(dataItem){
				var m= $("<li>").text(dataItem.user.firstName + " " + dataItem.user.lastName + ' said: ' + dataItem.message).css('list-style', 'none');
				var window = $('<div id="messageItem">').append(m);
				$('.chat').prepend(window);
			});
		}
	}

	$('#edit').on('click', function () {
		overlay.show();
		popupForEdit.toggle(300);
		changeUserName();
	});

	function changeUserName(){
		$('#saveEditInfo').on('click', function() {
			var editedFirstName = $('#editFirstName').val();
			var editedLastName = $('#editLastName').val();
			$.ajax({
				url: url + 'profile',
				method: 'POST',
				data: {firstName: editedFirstName, lastName: editedLastName}
			}).then(function () {
				popupForEdit.toggle(300);
				overlay.hide();})
				.then(function(){
					getName();
					viewMessages();
				});
		});
	}

	$('#follow-user').on('click', function () {
		overlay.show();
		popupForFollow.toggle(500);
		postEmail();
	});

	function postEmail() {
		$('#addNewUser').on('click', function () {
		var emailToFollow = $('#emailForSignUp').val();
			$.ajax({
				url: url + "subscribe",
				method: 'POST',
				data: {email: emailToFollow}
			}).then(function () {
				popupForFollow.toggle(500);
				overlay.hide();
				showFollowers();
			})
		})
	}

	var showFollowers = function(){
		$.ajax({
			url: url + 'subscribe',
			method: 'GET',
			success: function(response){
				console.log(response);
			}
		});
	};


});